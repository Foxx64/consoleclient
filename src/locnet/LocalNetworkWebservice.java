
package locnet;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.7-b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "LocalNetworkWebservice", targetNamespace = "http://example/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface LocalNetworkWebservice {


    /**
     * 
     * @return
     *     returns java.util.List<locnet.Computer>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getAllComputer", targetNamespace = "http://example/", className = "locnet.GetAllComputer")
    @ResponseWrapper(localName = "getAllComputerResponse", targetNamespace = "http://example/", className = "locnet.GetAllComputerResponse")
    @Action(input = "http://example/LocalNetworkWebservice/getAllComputerRequest", output = "http://example/LocalNetworkWebservice/getAllComputerResponse")
    public List<Computer> getAllComputer();

    /**
     * 
     * @param arg0
     */
    @WebMethod
    @RequestWrapper(localName = "addComputer", targetNamespace = "http://example/", className = "locnet.AddComputer")
    @ResponseWrapper(localName = "addComputerResponse", targetNamespace = "http://example/", className = "locnet.AddComputerResponse")
    @Action(input = "http://example/LocalNetworkWebservice/addComputerRequest", output = "http://example/LocalNetworkWebservice/addComputerResponse")
    public void addComputer(
        @WebParam(name = "arg0", targetNamespace = "")
        Computer arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns locnet.Computer
     * @throws EntityNotFoundException_Exception
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getComputerById", targetNamespace = "http://example/", className = "locnet.GetComputerById")
    @ResponseWrapper(localName = "getComputerByIdResponse", targetNamespace = "http://example/", className = "locnet.GetComputerByIdResponse")
    @Action(input = "http://example/LocalNetworkWebservice/getComputerByIdRequest", output = "http://example/LocalNetworkWebservice/getComputerByIdResponse", fault = {
        @FaultAction(className = EntityNotFoundException_Exception.class, value = "http://example/LocalNetworkWebservice/getComputerById/Fault/EntityNotFoundException")
    })
    public Computer getComputerById(
        @WebParam(name = "arg0", targetNamespace = "")
        Integer arg0)
        throws EntityNotFoundException_Exception
    ;

}
