
package locnet;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the locnet package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetComputerByIdResponse_QNAME = new QName("http://example/", "getComputerByIdResponse");
    private final static QName _AddComputerResponse_QNAME = new QName("http://example/", "addComputerResponse");
    private final static QName _GetComputerById_QNAME = new QName("http://example/", "getComputerById");
    private final static QName _GetAllComputerResponse_QNAME = new QName("http://example/", "getAllComputerResponse");
    private final static QName _GetAllComputer_QNAME = new QName("http://example/", "getAllComputer");
    private final static QName _EntityNotFoundException_QNAME = new QName("http://example/", "EntityNotFoundException");
    private final static QName _AddComputer_QNAME = new QName("http://example/", "addComputer");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: locnet
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddComputerResponse }
     * 
     */
    public AddComputerResponse createAddComputerResponse() {
        return new AddComputerResponse();
    }

    /**
     * Create an instance of {@link GetComputerById }
     * 
     */
    public GetComputerById createGetComputerById() {
        return new GetComputerById();
    }

    /**
     * Create an instance of {@link GetComputerByIdResponse }
     * 
     */
    public GetComputerByIdResponse createGetComputerByIdResponse() {
        return new GetComputerByIdResponse();
    }

    /**
     * Create an instance of {@link GetAllComputer }
     * 
     */
    public GetAllComputer createGetAllComputer() {
        return new GetAllComputer();
    }

    /**
     * Create an instance of {@link EntityNotFoundException }
     * 
     */
    public EntityNotFoundException createEntityNotFoundException() {
        return new EntityNotFoundException();
    }

    /**
     * Create an instance of {@link AddComputer }
     * 
     */
    public AddComputer createAddComputer() {
        return new AddComputer();
    }

    /**
     * Create an instance of {@link GetAllComputerResponse }
     * 
     */
    public GetAllComputerResponse createGetAllComputerResponse() {
        return new GetAllComputerResponse();
    }

    /**
     * Create an instance of {@link Computer }
     * 
     */
    public Computer createComputer() {
        return new Computer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetComputerByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://example/", name = "getComputerByIdResponse")
    public JAXBElement<GetComputerByIdResponse> createGetComputerByIdResponse(GetComputerByIdResponse value) {
        return new JAXBElement<GetComputerByIdResponse>(_GetComputerByIdResponse_QNAME, GetComputerByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddComputerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://example/", name = "addComputerResponse")
    public JAXBElement<AddComputerResponse> createAddComputerResponse(AddComputerResponse value) {
        return new JAXBElement<AddComputerResponse>(_AddComputerResponse_QNAME, AddComputerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetComputerById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://example/", name = "getComputerById")
    public JAXBElement<GetComputerById> createGetComputerById(GetComputerById value) {
        return new JAXBElement<GetComputerById>(_GetComputerById_QNAME, GetComputerById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllComputerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://example/", name = "getAllComputerResponse")
    public JAXBElement<GetAllComputerResponse> createGetAllComputerResponse(GetAllComputerResponse value) {
        return new JAXBElement<GetAllComputerResponse>(_GetAllComputerResponse_QNAME, GetAllComputerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllComputer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://example/", name = "getAllComputer")
    public JAXBElement<GetAllComputer> createGetAllComputer(GetAllComputer value) {
        return new JAXBElement<GetAllComputer>(_GetAllComputer_QNAME, GetAllComputer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntityNotFoundException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://example/", name = "EntityNotFoundException")
    public JAXBElement<EntityNotFoundException> createEntityNotFoundException(EntityNotFoundException value) {
        return new JAXBElement<EntityNotFoundException>(_EntityNotFoundException_QNAME, EntityNotFoundException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddComputer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://example/", name = "addComputer")
    public JAXBElement<AddComputer> createAddComputer(AddComputer value) {
        return new JAXBElement<AddComputer>(_AddComputer_QNAME, AddComputer.class, null, value);
    }

}
